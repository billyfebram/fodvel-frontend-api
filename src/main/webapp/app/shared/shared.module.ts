import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FodvelCmsSharedLibsModule, FodvelCmsSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [FodvelCmsSharedLibsModule, FodvelCmsSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [FodvelCmsSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FodvelCmsSharedModule {
  static forRoot() {
    return {
      ngModule: FodvelCmsSharedModule
    };
  }
}
